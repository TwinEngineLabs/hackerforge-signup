class CreateStripeCharges < ActiveRecord::Migration
  def change
    create_table :stripe_charges do |t|

      t.string :remote_charge_identifier

      t.string :stripe_customer_identifier

      t.integer :applicant_id

      t.string :status

      t.string :payment_type

      t.text :charge_json

      t.text :failure_information

      t.timestamps
    end
  end
end
