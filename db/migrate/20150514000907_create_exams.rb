class CreateExams < ActiveRecord::Migration
  def change
    create_table :exams do |t|
      t.string :title
      t.text :explanation_html
      t.text :special_information_html

      t.timestamps
    end
  end
end
