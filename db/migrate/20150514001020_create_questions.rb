class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :question_html

      t.text :explanation_html

      t.string :answer_type

      t.integer :order

      t.timestamps
    end
  end
end
