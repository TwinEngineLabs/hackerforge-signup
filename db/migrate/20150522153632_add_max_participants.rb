class AddMaxParticipants < ActiveRecord::Migration
  def change
    add_column :cohorts, :max_participants_without_equipment, :integer, :default => 15
    add_column :cohorts, :max_participants_with_equipment, :integer, :default => 5
  end
end
