class AddPaymentInformationToApplicant < ActiveRecord::Migration
  def change
    add_column :applicants, :stripe_payment_token, :string
    add_column :applicants, :stripe_last_four, :string
    add_column :applicants, :stripe_email, :string
    add_column :applicants, :stripe_plan, :string
    add_column :applicants, :stripe_customer_id, :string
  end
end
