class CreateCourses < ActiveRecord::Migration
  def self.up
    create_table :courses do |t|
      t.string :name

      t.text :html_description

      t.text :pricing_html

      t.text :schedule_html

      t.string :status

      t.timestamps
    end

    add_attachment :courses, :cover_image
  end

  def self.down
    drop_table :courses
  end
end
