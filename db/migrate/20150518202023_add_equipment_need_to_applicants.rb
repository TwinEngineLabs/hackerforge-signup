class AddEquipmentNeedToApplicants < ActiveRecord::Migration
  def change
    add_column :applicants, :needs_equipment, :boolean, :default => true
  end
end
