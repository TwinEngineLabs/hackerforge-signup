class CreateApplicants < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.integer :signup_id
      t.integer :cohort_id

      t.string :name
      t.string :provider
      t.string :uid

      t.string :app_user_link
      t.string :facebook_id
      t.string :github_profile
      t.string :linkedin_profile

      # facebook information
      t.datetime :birthday
      t.text :education
      t.string :email
      t.string :gender
      t.string :hometown
      t.boolean :verified, :default => false

      t.string :profile_image_url

      t.string :status

      t.timestamps
    end
  end
end
