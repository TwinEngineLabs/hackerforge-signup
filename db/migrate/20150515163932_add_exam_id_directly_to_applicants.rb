class AddExamIdDirectlyToApplicants < ActiveRecord::Migration
  def change
    add_column :applicants, :exam_id, :integer
  end
end
