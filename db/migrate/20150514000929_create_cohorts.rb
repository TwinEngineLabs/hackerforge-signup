class CreateCohorts < ActiveRecord::Migration
  def change
    create_table :cohorts do |t|
      t.integer :course_id
      
      t.datetime :start_date
      t.datetime :end_date

      t.text :schedule_html
      t.text :extra_information_html

      t.string :status

      t.timestamps
    end
  end
end
