class AddRemainingBalanceToApplicant < ActiveRecord::Migration
  def change
    add_column :applicants, :total_course_cost, :integer, :default => 2000
    add_column :applicants, :paid_in_full_at, :datetime
    add_column :applicants, :plan_type, :string #subscription, full_payment
    add_column :applicants, :last_payment_received, :datetime
    remove_column :applicants, :stripe_plan
  end
end
