class AddAnswerToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :answer, :text
    add_column :answers, :correct, :boolean
  end
end
