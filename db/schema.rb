# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150723165706) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "answers", force: true do |t|
    t.integer  "applicant_id"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "answer"
    t.boolean  "correct"
  end

  create_table "applicants", force: true do |t|
    t.integer  "signup_id"
    t.integer  "cohort_id"
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
    t.string   "app_user_link"
    t.string   "facebook_id"
    t.string   "github_profile"
    t.string   "linkedin_profile"
    t.datetime "birthday"
    t.text     "education"
    t.string   "email"
    t.string   "gender"
    t.string   "hometown"
    t.boolean  "verified",              default: false
    t.string   "profile_image_url"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "exam_id"
    t.boolean  "needs_equipment",       default: true
    t.string   "stripe_payment_token"
    t.string   "stripe_last_four"
    t.string   "stripe_email"
    t.string   "stripe_customer_id"
    t.integer  "total_course_cost",     default: 2000
    t.datetime "paid_in_full_at"
    t.string   "plan_type"
    t.datetime "last_payment_received"
    t.boolean  "feature",               default: false
  end

  create_table "cohorts", force: true do |t|
    t.integer  "course_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.text     "schedule_html"
    t.text     "extra_information_html"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "max_participants_without_equipment", default: 15
    t.integer  "max_participants_with_equipment",    default: 5
  end

  create_table "courses", force: true do |t|
    t.string   "name"
    t.text     "html_description"
    t.text     "pricing_html"
    t.text     "schedule_html"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cover_image_file_name"
    t.string   "cover_image_content_type"
    t.integer  "cover_image_file_size"
    t.datetime "cover_image_updated_at"
  end

  create_table "exams", force: true do |t|
    t.string   "title"
    t.text     "explanation_html"
    t.text     "special_information_html"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "course_id"
  end

  create_table "questions", force: true do |t|
    t.text     "question_html"
    t.text     "explanation_html"
    t.string   "answer_type"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "exam_id"
    t.text     "correct_answer"
  end

  create_table "signups", force: true do |t|
    t.string   "email_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stripe_charges", force: true do |t|
    t.string   "remote_charge_identifier"
    t.string   "stripe_customer_identifier"
    t.integer  "applicant_id"
    t.string   "status"
    t.string   "payment_type"
    t.text     "charge_json"
    t.text     "failure_information"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
