class ExamsController < ApplicationController
  before_filter :load_objects

  def show
    @applicant.begin_exam! if @applicant.pending_exam?  
    @applicant.exam = @exam

    @question = @exam.questions.
      includes(:answers).references(:answers).
      where("answers.applicant_id = ? AND answers.answer IS NULL", @applicant.id).first

    if @question.nil? && @applicant.exam_in_progress?
      @applicant.complete_exam!

      redirect_to course_cohort_applicant_path(@course, @cohort, @applicant)
    else
      @answer = Answer.new
      @answer.applicant = @applicant
      @answer.question = @question
    end
  end

  def load_objects
    @applicant = Applicant.find(session[:applicant_id])
    @exam = Exam.find(params[:id])
    @course = @exam.course
    @cohort = @applicant.cohort
  end
end
