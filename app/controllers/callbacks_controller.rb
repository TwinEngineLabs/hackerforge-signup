class CallbacksController < ApplicationController
  def facebook
    info_hash = request.env['omniauth.auth']["extra"]["raw_info"]

    info_hash["image"] = request.env['omniauth.auth']["info"]["image"]
    info_hash["cohort_id"] = session[:cohort_id] 

    applicant = Applicant.find_or_create_from_info_hash(info_hash)

    if session[:cohort_id].blank?
      @cohort = applicant.cohort
    else
      @cohort = Cohort.find(session[:cohort_id]) 
    end

    @course = @cohort.course


    session["applicant_id"] = applicant.id

    if applicant.awaiting_new_cohort?
      applicant.accept_applicant!
    end

    redirect_to course_cohort_applicant_path(@course, @cohort, applicant)
  end
end
