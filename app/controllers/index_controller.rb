class IndexController < ApplicationController
	
	def index
		@title = "HackerForge"
		render :signup
	end

  def signup
		@title = "HackerForge"

    unless params["email"].blank?
      Signup.create(:email_address => params[:email])
      render :thanks
    else
      render :signup
    end
  end

  def faq
    @title = "HackerForge"
  end
end
