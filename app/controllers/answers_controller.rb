class AnswersController < ApplicationController
  before_filter :load_objects

  def create
    answer = Answer.where(:applicant_id => @applicant.id, :question_id => @question.id).first
    answer.update_attributes(params[:answer].permit(:answer))

    answer.save

    redirect_to course_cohort_applicant_path(@course, @cohort, @applicant)
  end

  def load_objects
    @applicant = Applicant.find(session[:applicant_id])
    @exam = Exam.find(params[:exam_id])
    @course = Course.find(params[:course_id])
    @cohort = @applicant.cohort
    @question = Question.find(params[:question_id])
  end
end
