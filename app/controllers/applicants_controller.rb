class ApplicantsController < ApplicationController
  before_filter :load_objects
  before_filter :authenticate, :except => :new

  def authenticate
    if session[:applicant_id].blank?
      redirect_to "/auth/facebook" and return 
    end
  end

  def new
    @applicant = Applicant.new

    session[:cohort_id] = @cohort.id

    render :index
  end

  def show
    @exam = @course.exam
    @applicant.exam = @exam

    session[:cohort_id] = @applicant.cohort.id if @applicant.cohort

    @applicant.save

    if @applicant && @applicant.exam_in_progress?
      redirect_to course_exam_path(@course, @exam)
    end
  end

  def has_equipment
    @applicant.needs_equipment = false
    @applicant.save

    redirect_to course_cohort_applicant_path(@course, @cohort, @applicant), :notice => "Successfully updated your equipment preferences."
  end

  def needs_equipment
    @applicant.needs_equipment = true
    @applicant.save

    redirect_to course_cohort_applicant_path(@course, @cohort, @applicant), :notice => "Successfully updated your equipment preferences."
  end

  def load_objects
    unless params[:course_id].blank?
      session[:course_id] = params[:course_id]
      @course = Course.find(params[:course_id])
    end

    unless params[:cohort_id].blank?
      session[:cohort_id] = params[:cohort_id]
      @cohort = Cohort.find(params[:cohort_id])
    end
    
    unless params[:id].blank?
      @applicant = Applicant.find(params[:id])
    end

    unless params[:applicant_id].blank?
      @applicant = Applicant.find(params[:applicant_id])
    end
  end
end
