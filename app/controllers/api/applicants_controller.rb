class Api::ApplicantsController < ActionController::Base

  def submit_charge
    @applicant = Applicant.find(params[:id])

    token = params["token"]
    payment_type = params["type"]
    
    amount = payment_type == "subscription" ? 25_000 : 200_000
    description = payment_type == "subscription" ? "Twice Monthly Payment for Hackerforge.io" : "Full Course Tuition for Hackerforge.io"

    token_id = params["token"]["id"]


    if payment_type == "subscription"
      begin
        customer = Stripe::Customer.create(
          :source => token_id,
          :description => "Applicant ##{@applicant.id} - #{@applicant.email}",
          :email => @applicant.email,
          :plan => "twice_monthly"
        ) 

        @applicant.begin_subscription!

      rescue Exception => e
        binding.pry
        render :json => {:success => false, :error => "Please report the following information to the HackerForge owners: #{e.to_s}"} and return
      end
    else
      begin
        customer = Stripe::Customer.create(
          :source => token_id,
          :description => "Applicant ##{@applicant.id} - #{@applicant.email}",
          :email => @applicant.email
        ) 
        
        charge = Stripe::Charge.create(
          :amount => amount,
          :currency => "USD",
          :customer => customer.id,
          :description => description,
          :capture => true,
          :receipt_email => @applicant.email
        )

        if charge.paid
          stripe_charge = StripeCharge.new
          stripe_charge.charge_json = charge.to_json
          stripe_charge.remote_charge_identifier = charge.id
          stripe_charge.stripe_customer_identifier = charge.customer
          stripe_charge.applicant_id = @applicant.id
          stripe_charge.payment_type = payment_type 
          stripe_charge.save

          @applicant.pay_in_full!
        else
          render :json => {:success => false, :error => "The charge to your credit card failed. You may need to speak with your bank if you think this is in error."} and return 
        end
      rescue Stripe::CardError => e
        render :json => {:success => false, :error => "Please report the following information to the HackerForge owners: #{e.to_s}"} and return
      end
    end

    render :json => {:success => true, :error => nil, :redirect_to => course_cohort_applicant_path(@applicant.cohort.course, @applicant.cohort, @applicant)} and return
  end

end
