ActiveAdmin.register Course do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :cover_image, :name, :status, :html_description, :pricing_html, :schedule_html

  form :html => {:multipart => true} do |f|
    f.semantic_errors

    f.inputs do 
      f.input :cover_image, :as => :file
      f.input :name
      f.input :status
      f.input :html_description
      f.input :pricing_html
      f.input :schedule_html
    end

    f.actions
  end
end
