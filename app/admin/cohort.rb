ActiveAdmin.register Cohort do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :start_date, :end_date, :course_id, :schedule_html, :extra_information_html, :status, :max_participants_without_equipment, :max_participants_with_equipment
end
