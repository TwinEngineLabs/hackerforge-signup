ActiveAdmin.register Applicant do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  
  member_action :pass_student
  member_action :fail_student

  permit_params :cohort_id, :exam_id, :name, :provider, :uid, :app_user_link, :github_profile, 
    :linkedin_profile, :birthday, :education, :email, :gender, :hometown, :verified, :profile_image_url, :status, :needs_equipment,
    :stripe_payment_token, :stripe_last_four, :stripe_email, :stripe_plan


  Applicant.aasm.events.each do |aasm_event|
    action = aasm_event.name

    member_action action do
      @applicant = Applicant.find(params[:id])
      @applicant.send(action.to_s + "!")
      redirect_to admin_applicant_path(@applicant), :notice => "Action Successful"
    end

  end

  index do
    selectable_column
    column :name    

    column :status
    column :stripe_plan

    column :needs_equipment
    
    column :gender

    column "facebook" do |applicant|
      link_to applicant.app_user_link, applicant.app_user_link
    end    

    column :birthday
    column :education
    column :hometown


    actions dropdown: true do |applicant|
      applicant.aasm.events.each do |event|
        item event.name.to_s.titleize, send(event.name.to_s + "_admin_applicant_path", applicant), :class => "member_link"
      end
    end

  end

end
