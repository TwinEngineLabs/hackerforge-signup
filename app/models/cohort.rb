class Cohort < ActiveRecord::Base
  include AASM

  belongs_to :course

  has_many :applicants


  aasm :column => 'status' do
    state :registrations_open, :initial => true

    state :registrations_closed

    state :in_progress

    state :job_fair_in_progress

    state :graduated

    event :close_registrations do
      transitions :from => :registrations_open, :to => :registrations_closed
    end

    event :commence do
      transitions :from => :registrations_closed, :to => :in_progress
    end

    event :begin_job_fair do
      transitions :from => :in_progress, :to => :job_fair_in_progress
    end

    event :graduate_class do
      transitions :from => :job_fair_in_progress, :to => :graduated
    end

  end

  def full?
    self.applicants.accepted_students.count >= self.max_participants_without_equipment + self.max_participants_with_equipment
  end

end
