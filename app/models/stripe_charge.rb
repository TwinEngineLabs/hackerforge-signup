class StripeCharge < ActiveRecord::Base
  include AASM

  aasm :column => 'status' do
    state :charge_successful, :initial => true
    state :subscribed
    state :failed_subscription

    event :subscribe_customer do
      transitions from: :charge_succesful, to: :subscribed, guard: :attempt_subscribe
    end
  end

  def attempt_subscribe
    if self.payment_type == "subscription"
      charge = Stripe::Charge.create(
        :amount => amount,
        :currency => "USD",
        :customer => customer.id,
        :description => description,
        :capture => false,
        :receipt_email => @applicant.email
      ) 
    end
  end
end
