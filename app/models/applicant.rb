class Applicant < ActiveRecord::Base
  include AASM

  belongs_to :signup

  belongs_to :cohort
  validates_presence_of :cohort

  has_many :answers

  belongs_to :exam

  scope :students_without_equipment, -> { where(:needs_equipment => true, :status => 'accepted_into_cohort') }

  scope :students_with_equipment, -> { where(:needs_equipment => false, :status => 'accepted_into_cohort') }

  scope :accepted_students, -> { self.where(:status => 'accepted_into_cohort') }

  aasm :column => 'status' do
    state :pending_exam, :initial => true
    state :exam_in_progress
    state :exam_complete

    state :failed

    state :awaiting_payment

    state :subscribed
    state :paid_in_full

    state :accepted_into_cohort

    state :awaiting_new_cohort

    state :rejected_new_cohort

    event :begin_exam do
      transitions :from => :pending_exam, :to => :exam_in_progress

      after do
        self.exam.questions.each do |question|
          Answer.create(:question_id => question, :applicant_id => self.id, :question_id => question.id) 
        end
      end
    end

    event :complete_exam do
      transitions :from => :exam_in_progress, :to => :exam_complete
    end

    event :pass_applicant do
      transitions :from => :exam_complete, :to => :awaiting_payment
    end

    event :fail_applicant do
      transitions :from => :exam_complete, :to => :failed
    end

    event :begin_subscription do
      transitions :from => :awaiting_payment, :to => :subscribed

      after do
        self.plan_type = "subscription"
        self.last_payment_received = Time.now
        self.save

        self.accept_applicant!
      end
    end

    event :pay_in_full do
      transitions :from => :awaiting_payment, :to => :paid_in_full

      after do
        self.plan_type = "full_payment"
        self.last_payment_received = Time.now
        self.paid_in_full_at = Time.now
        self.save

        self.accept_applicant!
      end
    end

    event :accept_applicant do
      transitions :from => [:subscribed, :paid_in_full], :to => :accepted_into_cohort do
        guard do
          !self.cohort.full?
        end
      end

      transitions :from => [:subscribed, :paid_in_full], :to => :awaiting_new_cohort
    end

    event :postpone_applicant do
      transitions :from => [:subscribed, :paid_in_full], :to => :awaiting_new_cohort
    end

    event :applicant_rejected_cohorts do
      transitions :from => :awaiting_new_cohort, :to => :rejected_new_cohort
    end

  end

  def self.find_or_create_from_info_hash(info_hash)
    applicant = Applicant.find_by_facebook_id(info_hash["id"])

    if applicant.nil?
      applicant = Applicant.new
    end   

    applicant.facebook_id = info_hash["id"]
    applicant.profile_image_url = info_hash["image"]
    applicant.name = info_hash["name"]
    applicant.education = info_hash["education"].map {|ed| "#{ed["type"]} - #{ed["school"]["name"]}" }.uniq.join(",") unless info_hash["education"].blank?
    applicant.gender = info_hash["gender"]
    applicant.hometown = info_hash["hometown"]["name"] unless info_hash["hometown"].blank?
    applicant.app_user_link = info_hash["link"]
    applicant.birthday = Date.strptime(info_hash["birthday"], "%m/%d/%Y") unless info_hash["birthday"].blank?
    applicant.verified = info_hash["verified"] && info_hash["verified"] == true ? true : false
    applicant.email = info_hash["email"]
    applicant.provider = "facebook"
    applicant.uid = info_hash["id"]
    applicant.cohort_id = info_hash["cohort_id"] unless info_hash["cohort_id"].blank?

    unless applicant.email.blank? || (signup = Signup.find_by_email_address(applicant.email)).blank?
      applicant.signup = signup
    end

    applicant.save

    return applicant
  end
end
