class Course < ActiveRecord::Base
  include AASM

  has_one :exam
  has_many :cohorts

  aasm :column => 'status' do
    state :inactive, :initial => true

    state :prelaunch

    state :active

    event :prelaunch do 
      transitions :from => :inactive, :to => :prelaunch
    end

    event :activate do
      transitions :from => [:inactive, :prelaunch], :to => :active
    end 

    event :deactivate do
      transitions :from => [:prelaunch, :active], :to => :inactive
    end
  end

  def cohorts_available?
    false
  end

  has_attached_file :cover_image, :styles => { :main => "1200x200" }
  validates_attachment :cover_image, content_type: { content_type:     ["image/jpg", "image/jpeg", "image/png"] }
end
